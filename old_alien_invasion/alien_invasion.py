import sys
import pygame
from pygame.sprite import Group
from settings import Settings
from ship import Ship
import game_functions as gf

def run_grame():
    # Initialize game and create a screen object
    #print ('Starting ...')
    pygame.init()
    ai_settings = Settings()
    screen = pygame.display.set_mode((ai_settings.screen_with, ai_settings.screen_height))
    pygame.display.set_caption("Alien Invasion")

    # Make a ship
    ship = Ship(ai_settings,screen)
    # Make a group to store bullets in
    bullets = Group()

    # Set the background color
    #bg_color = (230,230,230)

    # Start the main loop for the game
    while True:
        #print ('Entered while loop in run_grame')
        gf.check_events(ai_settings, screen, ship, bullets)
        #gf.check_events(ship)
        #print ('calling ship.update() in run_grame')
        ship.update()
        gf.update_bullets(bullets)
        #print ('calling gf.update_screen in run_grame')
        gf.update_screen(ai_settings,screen,ship, bullets)
        #print ('called gf.update_screen in run_grame')


#print ('calling run_grame')
run_grame()
